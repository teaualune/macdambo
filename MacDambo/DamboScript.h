//
//  DamboScript.h
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/26.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGTDambo.h"

@interface DamboScript : NSObject

@property (nonatomic, readonly) int currentFrame;

- (BOOL)performScript:(OGTDambo *)dambo;
- (void)loadMovie:(NSString *)name;

@end
