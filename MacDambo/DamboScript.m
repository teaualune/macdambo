//
//  DamboScript.m
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/26.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import "DamboScript.h"

typedef struct _DamboMove {
    unsigned char bodyCode;
    int offset;
} DamboMove;

#define SCRIPT_LENGTH 128

@interface DamboScript ()
{
    NSMutableArray *script;
}

- (DamboMove)moveFromSpaceString:(NSString *)s;

@end

@implementation DamboScript

- (id)init
{
    self = [super init];
    if (self) {
        _currentFrame = 0;
        script = [[NSMutableArray alloc] init];
    }
    return self;
}

- (BOOL)performScript:(OGTDambo *)dambo
{
    if (_currentFrame < [script count]) {
        NSMutableArray *moves = script[_currentFrame];
        for (int i = 0; i < [moves count]; i ++) {
            DamboMove move = [self moveFromSpaceString:moves[i]];
            [dambo moveBody:move.bodyCode offset:move.offset];
        }
        _currentFrame ++;
        return YES;
    } else {
        _currentFrame = 0;
        return NO;
    }
}

- (void)loadMovie:(NSString *)name
{
    [script removeAllObjects];
    FILE *f = fopen([name UTF8String], "r");
    char line[SCRIPT_LENGTH];
    NSArray *data;
    while (fgets(line, SCRIPT_LENGTH, f) != NULL) {
        data = [[NSString stringWithCString:line encoding:NSUTF8StringEncoding] componentsSeparatedByString:@","];
        [script addObject:[[NSMutableArray alloc] initWithArray:data]];
    }
}

- (DamboMove)moveFromSpaceString:(NSString *)s
{
    NSArray *a = [s componentsSeparatedByString:@" "];
    NSString *code = a[0], *offset = a[1];
    DamboMove m;
    m.bodyCode = *[code UTF8String];
    m.offset = [offset intValue];
    return m;
}

@end
