//
//  MDApplication.h
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/20.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

#import "OGTDambo.h"
#import "DamboScript.h"

#define WIDTH 800
#define HEIGHT 600

@interface MDApplication : NSObject

@property (nonatomic, strong) OGTDambo *dambo;
@property (nonatomic, strong) DamboScript *script;
@property (nonatomic, copy) NSString *scriptName;

- (void)startWithArgc:(int)argc argv:(const char * [])argv;
- (void)initialize;

@end
