//
//  MDApplication.m
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/20.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import "MDApplication.h"

#define DEFAULT_MOVE_OFFSET 2

const GLdouble INTERVAL = 60;

int moveOffset = DEFAULT_MOVE_OFFSET;

GLfloat gCubeVertexData[216] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ,     normalX, normalY, normalZ,
    0.5f, -0.5f, -0.5f,        1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,          1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    
    0.5f, 0.5f, -0.5f,         0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 1.0f, 0.0f,
    
    -0.5f, 0.5f, -0.5f,        -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        -1.0f, 0.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,       0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         0.0f, -1.0f, 0.0f,
    
    0.5f, 0.5f, 0.5f,          0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, 0.0f, 1.0f,
    
    0.5f, -0.5f, -0.5f,        0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 0.0f, -1.0f
};

id this;

double theta = 0;
double phi = 0;

int cursorX;
int cursorY;

@implementation MDApplication

- (void)initialize // Called before main loop to set up the program
{

    self.dambo = [[OGTDambo alloc] init];
    [self.dambo initPoses];

    self.script = [[DamboScript alloc] init];

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDownloadsDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    self.scriptName = [directory stringByAppendingPathComponent:@"movie.txt"];

    glClearColor(0.0, 0.0, 0.0, 0.0);
	glShadeModel(GL_SMOOTH);

	GLfloat mat_diffuse[] = {0.7, 0.7, 0.7, 1.0};
	GLfloat mat_ambient[] = {0.7, 0.7, 0.7, 1.0};
	GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat mat_shininess[] = {30.0};
	GLfloat light_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);

    this = self;
}

// Called at the start of the program, after a glutPostRedisplay() and during idle
// to display a frame
void display ()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    double a = 50 * cos(theta) * cos(phi);
    double b = 50 * sin(theta) * cos(phi);
    double c = 50 * sin(phi);
    gluLookAt(a, c, b, 0, 0, 0, 0, 0, 1);
    [[this dambo] drawDamboAtX:0 Y:0 Z:0];
    glutSwapBuffers();
}

// Called every time a window is resized to resize the projection matrix
void reshape (int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.1, 0.1, -h/(10.0*w), h/(10.0*w), 0.5, 1000.0);
    glMatrixMode(GL_MODELVIEW);
}

void keyboard (unsigned char key, int x, int y)
{
    switch (key) {
        case '=':
            // start animation
            [[this script] loadMovie:[this scriptName]];
            glutTimerFunc(0, onTimer, 1);
            break;
        default:
            [[this dambo] moveBody:key offset:moveOffset];
            break;
    }
    glutPostRedisplay();
}

void onTimer(int value) {
    if ([[this script] performScript:[this dambo]]) {
        glutTimerFunc(INTERVAL, onTimer, 1);
    }
    glutPostRedisplay();
}

void mouse (int button, int state, int x, int y)
{
    if (state == 0) {
        cursorX = x;
        cursorY = y;
    }
}

void motion (int x, int y)
{
    double xx = (double) (x - cursorX) / 300.0;
    double yy = (double) (y - cursorY) / 300.0;
    phi -= xx;
    theta -= yy;
    cursorX = x;
    cursorY = y;
}

void teardown ()
{
}

- (void)startWithArgc:(int)argc argv:(const char * [])argv
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);

    glutInitWindowSize(WIDTH, HEIGHT);

    glutInitWindowPosition(0, 0);

    glutCreateWindow("Hello!");

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);

    [self initialize];

    glutMainLoop();

}

- (void)dealloc
{
    teardown();
}

@end
