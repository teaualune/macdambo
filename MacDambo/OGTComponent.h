//
//  OGTComponent.h
//  OpenGLESTest2
//
//  Created by Teaualune Tseng on 2014/4/20.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

@interface OGTComponent : NSObject
{
    GLfloat offset[3];
    GLfloat pivot[3];
    GLint rotate[3];
    GLfloat scale[3];
    GLfloat color[3];
}

- (void)setOffsetX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z;
- (void)setPivotX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z;
- (void)setScaleX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z;
- (void)setRotateX:(GLint)x Y:(GLint)y Z:(GLint)z;
- (void)setColorR:(GLfloat)r G:(GLfloat)g B:(GLfloat)b;

- (void)setOffset:(GLfloat)x I:(int)i;
- (void)setPivot:(GLfloat)x I:(int)i;
- (void)setScale:(GLfloat)x I:(int)i;
- (void)setRotate:(GLfloat)x I:(int)i;
- (void)setColor:(GLfloat)r I:(int)i;

- (GLfloat)offset:(int)i;
- (GLfloat)pivot:(int)i;
- (GLfloat)scale:(int)i;
- (GLfloat)rotate:(int)i;
- (GLfloat)color:(int)i;

- (void)draw;
- (void)rotate;

@end
