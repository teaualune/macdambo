//
//  OGTComponent.m
//  OpenGLESTest2
//
//  Created by Teaualune Tseng on 2014/4/20.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import "OGTComponent.h"

@implementation OGTComponent

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)setOffsetX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z
{
    offset[0] = x;
    offset[1] = y;
    offset[2] = z;
}

- (void)setPivotX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z
{
    pivot[0] = x;
    pivot[1] = y;
    pivot[2] = z;
}

- (void)setRotateX:(GLint)x Y:(GLint)y Z:(GLint)z
{
    rotate[0] = x;
    rotate[1] = y;
    rotate[2] = z;
}

- (void)setScaleX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z
{
    scale[0] = x;
    scale[1] = y;
    scale[2] = z;
}

- (void)setColorR:(GLfloat)r G:(GLfloat)g B:(GLfloat)b
{
    color[0] = r;
    color[1] = g;
    color[2] = b;
}

- (void)setOffset:(GLfloat)x I:(int)i
{
    offset[i] = x;
}

- (void)setPivot:(GLfloat)x I:(int)i
{
    pivot[i] = x;
}

- (void)setRotate:(GLfloat)x I:(int)i
{
    rotate[i] = x;
}

- (void)setScale:(GLfloat)x I:(int)i
{
    scale[i] = x;
}

- (void)setColor:(GLfloat)r I:(int)i
{
    color[i] = r;
}

- (NSArray *)getColor
{
    return @[@(color[0]), @(color[1]), @(color[2])];
}

- (GLfloat)offset:(int)i
{
    return offset[i];
}

- (GLfloat)pivot:(int)i
{
    return pivot[i];
}

- (GLfloat)scale:(int)i
{
    return scale[i];
}

- (GLfloat)rotate:(int)i
{
    return rotate[i];
}

- (GLfloat)color:(int)i
{
    return color[i];
}

- (void)draw
{
    [self rotate];

    glPushMatrix();
    glColor4f(color[0], color[1], color[2], 1);
    glTranslatef(offset[0], offset[1], offset[2]);
    glScalef(scale[0], scale[1], scale[2]);
    glutSolidCube(1);
    glPopMatrix();
}

- (void)rotate
{
    glTranslatef(pivot[0], pivot[1], pivot[2]);
    glRotatef(rotate[0], 1, 0, 0);
    glRotatef(rotate[1], 0, 1, 0);
    glRotatef(rotate[2], 0, 0, 1);
    glTranslatef(-pivot[0], -pivot[1], -pivot[2]);
}


@end
