//
//  OGTDambo.h
//  OpenGLESTest2
//
//  Created by Teaualune Tseng on 2014/4/15.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#import "OGTComponent.h"
#import "OGTHead.h"

#define DAMBO_COLOR_R 0.8
#define DAMBO_COLOR_G 0.6
#define DAMBO_COLOR_B 0.3

@interface OGTDambo : NSObject

@property (nonatomic, strong) OGTHead *head;
@property (nonatomic, strong) OGTComponent *body;
@property (nonatomic, strong) OGTComponent *leftArm;
@property (nonatomic, strong) OGTComponent *rightArm;
@property (nonatomic, strong) OGTComponent *leftLeg;
@property (nonatomic, strong) OGTComponent *rightLeg;
@property (nonatomic, copy) NSArray *skirt;

- (void)drawDamboAtX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z;
- (void)initPoses;
- (void)moveBody:(unsigned char)bodyCode offset:(GLint)offset;

@end
