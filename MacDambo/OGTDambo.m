//
//  OGTDambo.m
//  OpenGLESTest2
//
//  Created by Teaualune Tseng on 2014/4/15.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import "OGTDambo.h"

#define COLLAPSE_SCALE_FACTOR 28.0

@implementation OGTDambo

- (id)init
{
    self = [super init];
    if (self) {
        _head = [[OGTHead alloc] init];
        _body = [[OGTComponent alloc] init];
        _leftArm = [[OGTComponent alloc] init];
        _rightArm = [[OGTComponent alloc] init];
        _leftLeg = [[OGTComponent alloc] init];
        _rightLeg = [[OGTComponent alloc] init];
        _skirt = @[
                   [[OGTComponent alloc] init],
                   [[OGTComponent alloc] init],
                   [[OGTComponent alloc] init],
                   [[OGTComponent alloc] init]
                   ];

        [_head setPivotX:0 Y:0 Z:1];
        [_head setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        [_body setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        [_leftArm setPivotX:0 Y:1.6 Z:1];
        [_leftArm setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        [_rightArm setPivotX:0 Y:-1.6 Z:1];
        [_rightArm setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        [_leftLeg setPivotX:0 Y:0.5 Z:-1.8];
        [_leftLeg setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        [_rightLeg setPivotX:0 Y:-0.5 Z:-1.8];
        [_rightLeg setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        // 0: front skirt
        [_skirt[0] setPivotX:0.1 Y:0 Z:-0.4];
        [_skirt[0] setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        // 1: right skirt
        [_skirt[1] setPivotX:0 Y:-0.1 Z:-0.4];
        [_skirt[1] setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        // 2: back skirt
        [_skirt[2] setPivotX:-0.1 Y:0 Z:-0.4];
        [_skirt[2] setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

        // 3: left skirt
        [_skirt[3] setPivotX:0 Y:0.1 Z:-0.4];
        [_skirt[3] setColorR:DAMBO_COLOR_R G:DAMBO_COLOR_G B:DAMBO_COLOR_B];

    }
    return self;
}

- (void)initPoses
{
    [_head setRotateX:0 Y:0 Z:0];
    [_head setScaleX:3 Y:4.5 Z:2.8];
    [_head setOffsetX:0 Y:0 Z:2.8];

    [_body setScaleX:1.8 Y:2.1 Z:2.8];
    [_body setOffsetX:0 Y:0 Z:0];

    [_leftArm setOffsetX:0 Y:1.6 Z:-0.6];
    [_leftArm setScaleX:0.8 Y:0.8 Z:4];
    [_leftArm setRotateX:2 Y:0 Z:0];

    [_rightArm setOffsetX:0 Y:-1.6 Z:-0.6];
    [_rightArm setScaleX:0.8 Y:0.8 Z:4];
    [_rightArm setRotateX:-2 Y:0 Z:0];

    [_leftLeg setOffsetX:0 Y:0.5 Z:-2.3];
    [_leftLeg setScaleX:1.8 Y:0.8 Z:1.8];
    [_leftLeg setRotateX:0 Y:0 Z:0];

    [_rightLeg setOffsetX:0 Y:-0.5 Z:-2.3];
    [_rightLeg setScaleX:1.8 Y:0.8 Z:1.8];
    [_rightLeg setRotateX:0 Y:0 Z:0];

    // 0: front skirt
    [_skirt[0] setOffsetX:1 Y:0 Z:-1.8];
    [_skirt[0] setScaleX:0.2 Y:2.1 Z:0.8];
    [_skirt[0] setRotateX:0 Y:-5 Z:0];
    
    // 1: right skirt
    [_skirt[1] setOffsetX:0 Y:-0.95 Z:-1.8];
    [_skirt[1] setScaleX:1.8 Y:0.2 Z:0.8];
    [_skirt[1] setRotateX:-5 Y:0 Z:0];
    
    // 2: back skirt
    [_skirt[2] setOffsetX:-1 Y:0 Z:-1.8];
    [_skirt[2] setScaleX:0.2 Y:2.1 Z:0.8];
    [_skirt[2] setRotateX:0 Y:5 Z:0];
    
    // 3: left skirt
    [_skirt[3] setOffsetX:0 Y:0.95 Z:-1.8];
    [_skirt[3] setScaleX:1.8 Y:0.2 Z:0.8];
    [_skirt[3] setRotateX:5 Y:0 Z:0];
}

- (void)drawDamboAtX:(GLfloat)x Y:(GLfloat)y Z:(GLfloat)z
{
    glPushMatrix();
    glTranslatef(x, y, z);

    glPushMatrix();
    [_body draw];
    glPopMatrix();

    glPushMatrix();
    [_head draw];
    glPopMatrix();

    glPushMatrix();
    [_leftArm draw];
    glPopMatrix();

    glPushMatrix();
    [_rightArm draw];
    glPopMatrix();

    glPushMatrix();
    [_leftLeg draw];
    glPopMatrix();

    glPushMatrix();
    [_rightLeg draw];
    glPopMatrix();

    for (OGTComponent *s in _skirt) {
        glPushMatrix();
        [s draw];
        glPopMatrix();
    }

    glPopMatrix();
}

- (void)moveBody:(unsigned char)bodyCode offset:(GLint)moveOffset
{
    switch (bodyCode) {
        case 'a':
            // head tilt right
            rotateComponent(_head, 0, moveOffset);
            break;
        case 'z':
            // head tilt left
            rotateComponent(_head, 0, -moveOffset);
            break;
        case 's':
            // head down
            rotateComponent(_head, 1, moveOffset);
            break;
        case 'x':
            // head up
            rotateComponent(_head, 1, -moveOffset);
            break;
        case 'd':
            // head turn left (horizontal)
            rotateComponent(_head, 2, moveOffset);
            break;
        case 'c':
            // head turn right (horizontal)
            rotateComponent(_head, 2, -moveOffset);
            break;
            
        case 'f':
            // left arm swing up
            rotateComponent(_leftArm, 0, moveOffset);
            break;
        case 'v':
            // left arm swing down
            rotateComponent(_leftArm, 0, -moveOffset);
            break;
        case 'g':
            // left arm swing back
            rotateComponent(_leftArm, 1, moveOffset);
            break;
        case 'b':
            // left arm swing forth
            rotateComponent(_leftArm, 1, -moveOffset);
            break;
            
        case 'h':
            // right arm swing down
            rotateComponent(_rightArm, 0, moveOffset);
            break;
        case 'n':
            // right arm swing up
            rotateComponent(_rightArm, 0, -moveOffset);
            break;
        case 'j':
            // right arm swing back
            rotateComponent(_rightArm, 1, moveOffset);
            break;
        case 'm':
            // right arm swing forth
            rotateComponent(_rightArm, 1, -moveOffset);
            break;
            
        case 'F':
            // left leg swing up
            rotateComponent(_leftLeg, 0, moveOffset);
            break;
        case 'V':
            // left leg swing down
            rotateComponent(_leftLeg, 0, -moveOffset);
            break;
        case 'G':
            // left leg swing back
            rotateComponent(_leftLeg, 1, moveOffset);
            break;
        case 'B':
            // left leg swing forth
            rotateComponent(_leftLeg, 1, -moveOffset);
            break;
            
        case 'H':
            // right leg swing down
            rotateComponent(_rightLeg, 0, moveOffset);
            break;
        case 'N':
            // right leg swing up
            rotateComponent(_rightLeg, 0, -moveOffset);
            break;
        case 'J':
            // right leg swing back
            rotateComponent(_rightLeg, 1, moveOffset);
            break;
        case 'M':
            // right leg swing forth
            rotateComponent(_rightLeg, 1, -moveOffset);
            break;

        case 'q':
            // open or close eye light
            _head.lighting = ![_head isLighting];
            break;

        case 'w': {
            // special movement
            // collapse dambo
            GLdouble offset = moveOffset / COLLAPSE_SCALE_FACTOR;

            moveComponent(_head, 2, -offset);

            scaleComponent(_body, 2, -offset);
            moveComponent(_skirt[0], 2, offset);
            moveComponent(_skirt[1], 2, offset);
            moveComponent(_skirt[2], 2, offset);
            moveComponent(_skirt[3], 2, offset);

            moveComponent(_leftArm, 2, offset);
            moveComponent(_rightArm, 2, offset);
            scaleComponent(_leftArm, 2, -offset);
            scaleComponent(_rightArm, 2, -offset);

            moveComponent(_leftLeg, 2, 2 * offset);
            moveComponent(_rightLeg, 2, 2 * offset);
            scaleComponent(_leftLeg, 2, -offset);
            scaleComponent(_rightLeg, 2, -offset);
            break;
        }
        case 'e': {
            // special movement
            // expand dambo
            GLdouble offset = moveOffset / COLLAPSE_SCALE_FACTOR;

            moveComponent(_head, 2, offset);

            scaleComponent(_body, 2, offset);
            moveComponent(_skirt[0], 2, -offset);
            moveComponent(_skirt[1], 2, -offset);
            moveComponent(_skirt[2], 2, -offset);
            moveComponent(_skirt[3], 2, -offset);

            moveComponent(_leftArm, 2, -offset);
            moveComponent(_rightArm, 2, -offset);
            scaleComponent(_leftArm, 2, offset);
            scaleComponent(_rightArm, 2, offset);

            moveComponent(_leftLeg, 2, -2 * offset);
            moveComponent(_rightLeg, 2, -2 * offset);
            scaleComponent(_leftLeg, 2, offset);
            scaleComponent(_rightLeg, 2, offset);
            break;
        }

        case '1':
            // reset
            [self initPoses];
            _head.lighting = NO;
            break;

        default:
            break;
    }
}

void rotateComponent (OGTComponent *c, int i, GLint x)
{
    [c setRotate:[c rotate:i] + x I:i];
}

void moveComponent (OGTComponent *c, int i, GLdouble x)
{
    [c setOffset:[c offset:i] + x I:i];
}

void scaleComponent (OGTComponent *c, int i, GLdouble x)
{
    [c setScale:[c scale:i] + x I:i];
}

@end
