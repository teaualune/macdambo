//
//  OGTHead.h
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/21.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OGTComponent.h"
#import <AppKit/AppKit.h>

@interface OGTHead : OGTComponent
{
    GLuint faceTexture;
    GLuint faceLightTexture;
}

@property (nonatomic, getter = isLighting) BOOL lighting;

@end
