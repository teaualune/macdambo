//
//  OGTHead.m
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/21.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#import "OGTHead.h"
#include "TextureLoader.h"

//typedef struct RGBAData {
//    Byte r;
//    Byte g;
//    Byte b;
//    Byte a;
//}RGBAData;
//
//typedef struct RGBData {
//    Byte r;
//    Byte g;
//    Byte b;
//}RGBData;

#define BITS_PER_COMPONENT 8
#define _32_BYTE_PER_PIXEL 4
#define _24_BYTE_PER_PIXEL 3

@implementation OGTHead

- (id)init
{
    self = [super init];
    if (self) {
        CGImageRef i = MyCreateCGImageFromFile(@"/Users/teaualune/Downloads/dambo_face.bmp");
        CGContextRef c = CreateARGBBitmapContext(i);
        faceTexture = GLTexture2DCreateFromContext(c);
//        faceTexture = [self textureFromImageNamed:@"/Users/teaualune/Downloads/dambo_face.bmp"];
//        faceTexture = loadTexture("/Users/teaualune/Downloads/dambo_face.bmp", 450, 280);
        faceLightTexture = loadTexture("/Users/teaualune/Downloads/dambo_face_light.bmp", 450, 280);
        _lighting = NO;
    }
    return self;
}

void drawEye(GLdouble x, GLdouble y, GLdouble z, GLdouble radius, BOOL isLighting) {
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslatef(x, y, z);
    if (isLighting) {
        glColor3f(1, 1, 0);
    } else {
        glColor3f(0, 0, 0);
    }
    gluDisk(gluNewQuadric(), 0, radius, 100, 1);
    glPopMatrix();
}

void drawMouth(GLdouble x, GLdouble y, GLdouble z, GLdouble length) {
    // (x, y, z) is the upper vertex of the mouth
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glTranslatef(x, y, z);
    glColor3f(0, 0, 0);
    glBegin(GL_TRIANGLES);
    glVertex3f(0, 0, 0);
    glVertex3f(length, -length, 0);
    glVertex3f(length, length, 0);
    glEnd();
    glPopMatrix();
}

- (void)draw
{
    [self rotate];
    
    glPushMatrix();
    glTranslatef(offset[0], offset[1], offset[2]);

    glPushMatrix();
    glScalef(scale[0], scale[1], scale[2]);
    glColor4f(color[0], color[1], color[2], 0.8);

//    glBegin(GL_QUADS);
//    glVertex3f(-0.5f, -0.5f,  0.5f);
//    glVertex3f( 0.5f, -0.5f,  0.5f);
//    glVertex3f( 0.5f,  0.5f,  0.5f);
//    glVertex3f(-0.5f,  0.5f,  0.5f);
//    glEnd();
//
//    glBegin(GL_QUADS);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//    glVertex3f(-0.5f,  0.5f, -0.5f);
//    glVertex3f( 0.5f,  0.5f, -0.5f);
//    glVertex3f( 0.5f, -0.5f, -0.5f);
//    glEnd();
//
//    glBegin(GL_QUADS);
//    glVertex3f(-0.5f,  0.5f, -0.5f);
//    glVertex3f(-0.5f,  0.5f,  0.5f);
//    glVertex3f( 0.5f,  0.5f,  0.5f);
//    glVertex3f( 0.5f,  0.5f, -0.5f);
//    glEnd();
//
//    glBegin(GL_QUADS);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//    glVertex3f( 0.5f, -0.5f, -0.5f);
//    glVertex3f( 0.5f, -0.5f,  0.5f);
//    glVertex3f(-0.5f, -0.5f,  0.5f);
//    glEnd();
//
//    // face
//    glBegin(GL_QUADS);
//    glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f, -0.5f, -0.5f);
//    glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f,  0.5f, -0.5f);
//    glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f,  0.5f,  0.5f);
//    glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f, -0.5f,  0.5f);
//    glEnd();
//
//    glBegin(GL_QUADS);
//    glVertex3f(-0.5f, -0.5f, -0.5f);
//    glVertex3f(-0.5f, -0.5f,  0.5f);
//    glVertex3f(-0.5f,  0.5f,  0.5f);
//    glVertex3f(-0.5f,  0.5f, -0.5f);
//    glEnd();

    glutSolidCube(1);
    glPopMatrix();

    drawEye(-0.35, -1, 1.51, 0.25, [self isLighting]);
    drawEye(-0.35, 1, 1.51, 0.25, [self isLighting]);
    drawMouth(0.4, 0, 1.51, 0.35);

    glPopMatrix();
}

- (CGContextRef)NSImageToCGContextRef:(NSImage *)image
{
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)[image TIFFRepresentation], NULL);
    CGImageRef imageRef =  CGImageSourceCreateImageAtIndex(source, 0, NULL);
    int width = CGImageGetWidth(imageRef);
    int height = CGImageGetHeight(imageRef);

    NSUInteger bytesPerRow = _32_BYTE_PER_PIXEL * width;
    NSUInteger bitsPerComponent = BITS_PER_COMPONENT;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *sourceData = malloc(height * width * _32_BYTE_PER_PIXEL);
    CGContextRef context = CGBitmapContextCreate(sourceData,
                                                 width,
                                                 height,
                                                 bitsPerComponent,
                                                 bytesPerRow,
                                                 colorSpace,
                                                 kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    return context;
}

- (GLuint)textureFromImageNamed:(NSString *)image
{
    NSImage *i = [NSImage imageNamed:image];
    if (i == nil) {
        NSLog(@"null image");
        return 0;
    }
    CGContextRef context = [self NSImageToCGContextRef:i];
    return GLTexture2DCreateFromContext(context);
}

// Create a 2D texture
GLuint GLTexture2DCreate(const GLuint nWidth,
                         const GLuint nHeight,
                         const GLvoid * const pPixels)
{
    GLuint nTID = 0;
    
    // Greate a texture
    glGenTextures(1, &nTID);
    
    if( nTID )
    {
		// Bind a texture with ID
        glBindTexture(GL_TEXTURE_2D, nTID);
        
        // Set texture properties (including linear mipmap)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
        // Initialize the texture
        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     GL_RGBA,
                     nWidth,
                     nHeight,
                     0,
                     GL_RGBA,
                     GL_UNSIGNED_INT_8_8_8_8_REV,
                     pPixels);
		
		// Generate mipmaps
		glGenerateMipmap(GL_TEXTURE_2D);
		
        // Discard
        glBindTexture(GL_TEXTURE_2D, 0);
    } // if
    
    return nTID;
} // GLTexture2DCreate

// Create a texture from a bitmap context
GLuint GLTexture2DCreateFromContext(CGContextRef pContext)
{
    GLuint nTID = 0;
    
    if( pContext != NULL )
    {
        GLuint nWidth  = GLuint(CGBitmapContextGetWidth(pContext));
        GLuint nHeight = GLuint(CGBitmapContextGetHeight(pContext));
        
        const GLvoid *pPixels = CGBitmapContextGetData(pContext);
        
        nTID = GLTexture2DCreate(nWidth, nHeight, pPixels);
        
        // Was there a GL error?
        GLenum nErr = glGetError();
        
        if( nErr != GL_NO_ERROR )
        {
            NSLog(@">> OpenGL Error: %04x caught at %s:%u", nErr, __FILE__, __LINE__);
			
			glDeleteTextures(1, &nTID);
			
			nTID = 0;
        } // if
    } // if
    
    return nTID;
} // GLTexture2DCreateFromContext

@end
