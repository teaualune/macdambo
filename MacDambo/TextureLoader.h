//
//  TextureLoader.h
//  MacDambo
//
//  Created by Teaualune Tseng on 2014/4/21.
//  Copyright (c) 2014年 Corrugater Studio. All rights reserved.
//

#ifndef MacDambo_TextureLoader_h
#define MacDambo_TextureLoader_h

GLuint loadTexture(const char * filename, const int width, const int height)
{
    GLuint texture;
    unsigned char * data;
    FILE *file = fopen(filename, "rb");

    if (file == NULL) {
        NSLog(@"no file");
        return 0;
    }

    data = (unsigned char *) malloc(width * height * 3);
    fread(data, width * height * 3, 1, file);
    fclose(file);
    NSLog(@"%lu", sizeof(data));

    for(int i = 0; i < width * height ; i ++) {
        int index = i * 3;
        unsigned char B, R;
        B = data[index];
        R = data[index + 2];
        data[index] = R;
        data[index + 2] = B;
    }

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data);
//    glTexImage2D(GL_TEXTURE_2D,
//                 0,
//                 GL_RGBA,
//                 width,
//                 height,
//                 0,
//                 GL_RGBA,
//                 GL_UNSIGNED_INT_8_8_8_8_REV,
//                 pPixels);
//    glGenerateMipmap(GL_TEXTURE_2D);
    free(data);

    return texture;
}


CGImageRef MyCreateCGImageFromFile (NSString* path)

{
    
    // Get the URL for the pathname passed to the function.
    
    NSURL             *url = [NSURL fileURLWithPath:path];
    
    CGImageRef        myImage = NULL;
    
    CGImageSourceRef  myImageSource;
    
    CFDictionaryRef   myOptions = NULL;
    
    CFStringRef       myKeys[2];
    
    CFTypeRef         myValues[2];
    
    
    
    // Set up options if you want them. The options here are for
    
    // caching the image in a decoded form and for using floating-point
    
    // values if the image format supports them.
    
    myKeys[0] = kCGImageSourceShouldCache;
    
    myValues[0] = (CFTypeRef)kCFBooleanTrue;
    
    myKeys[1] = kCGImageSourceShouldAllowFloat;
    
    myValues[1] = (CFTypeRef)kCFBooleanTrue;
    
    // Create the dictionary
    
    myOptions = CFDictionaryCreate(NULL, (const void **) myKeys,
                                   
                                   (const void **) myValues, 2,
                                   
                                   &kCFTypeDictionaryKeyCallBacks,
                                   & kCFTypeDictionaryValueCallBacks);

    // Create an image source from the URL.

    myImageSource = CGImageSourceCreateWithURL((__bridge CFURLRef) url, myOptions);
    CFRelease(myOptions);

    // Make sure the image source exists before continuing
    if (myImageSource == NULL) {
        fprintf(stderr, "Image source is NULL.");
        return  NULL;
    }
    myImage = CGImageSourceCreateImageAtIndex(myImageSource,
                                              0,
                                              NULL);
    CFRelease(myImageSource);
    if (myImage == NULL) {
        fprintf(stderr, "Image not created from image source.");
        return NULL;
    }
    
    return myImage;
}

CGContextRef CreateARGBBitmapContext(CGImageRef inImage)
{
    CGContextRef    context = NULL;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

#endif
