Mac Dambo
=========

Instructions
------------

1. Copy `movie.txt` to `Users/<your-name>/Downloads`
2. Run the application
3. Use mouse to drag view
4. Press different keys to operate Dambo. Commands can be found in `moveBody:offset:` method in `MacDambo/OGTDambo.m`
5. Press `=` to start movie
6. You can always press `1` to reset Dambo

